package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"strings"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/bhavin192/lsx-exporter/config"
	"gitlab.com/bhavin192/lsx-exporter/util"
)

var (
	namespace = "lsx"

	scrapeErrorsDesc = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "scrape_errors_total"),
		"Total number of errors querying the inverter.",
		nil,
		nil)
	upDesc = prometheus.NewDesc(
		prometheus.BuildFQName(namespace, "", "up"),
		"Was the last scrape of inverter successful.",
		nil,
		nil)
)

// inverterCollector is an exporter which collects metrics from an
// inverter. It implements prometheus.Collector interface.
type inverterCollector struct {
	// Inverter specifies an inverter client used for scraping
	// metrics.
	Inverter inverter

	// Directories specifies all the supported metrics by the
	// Inverter.
	Directories []config.Directory

	// RegisterGroups specifies the groups of directories to
	// scrape. Each registerGroup can specify all or a subset of
	// Directories.
	RegisterGroups []config.RegisterGroup

	// scrapeErrors specifies total errors occurred while scraping
	// Inverter.
	scrapeErrors int

	mu sync.Mutex
}

func (ic *inverterCollector) Describe(ch chan<- *prometheus.Desc) {
	for _, directory := range ic.Directories {
		for _, metric := range directory.Items {
			constLabels, variableLabels, _ := splitLabels(metric.Labels)
			name := prometheus.BuildFQName(namespace, directory.Directory, metric.MetricName)
			ch <- prometheus.NewDesc(name, metric.MetricHelp, variableLabels, constLabels)
		}
	}
	ch <- scrapeErrorsDesc
	ch <- upDesc
}

func (ic *inverterCollector) Collect(ch chan<- prometheus.Metric) {
	// Make metric collection concurrency safe.
	ic.mu.Lock()
	defer ic.mu.Unlock()

	up := ic.Scrape(ch)

	ch <- prometheus.MustNewConstMetric(
		scrapeErrorsDesc,
		prometheus.CounterValue,
		float64(ic.scrapeErrors),
	)
	ch <- prometheus.MustNewConstMetric(upDesc, prometheus.GaugeValue, up)
}

func (ic *inverterCollector) Scrape(ch chan<- prometheus.Metric) float64 {
	up := 1.0
	for _, regGroup := range ic.RegisterGroups {
		resp, err := ic.Inverter.GetRegisters(regGroup.FnCode, regGroup.StartAddr, regGroup.EndAddr)
		if err != nil {
			ic.scrapeErrors++
			up = 0.0
			// TODO: make it a warning
			fmt.Printf("failed to get register values for group %s: %v\n", regGroup.Name, err)
			continue
		}
		for _, directory := range ic.Directories {
			if !util.StringInSlice(directory.Directory, regGroup.Directories) {
				continue
			}
			for _, metric := range directory.Items {
				address, _ := hex.DecodeString(metric.Registers[0][2:])
				adderssInt := binary.BigEndian.Uint16(address)
				// Each output register is of 16bits, so when
				// we say 0x0000 that means first two numbers
				// of the reply part of complete response
				start := 28 + 2*int(adderssInt-regGroup.StartAddr)
				end := start + 2*len(metric.Registers)
				var val float64
				var asciiVal string
				if metric.ParserRule != "ascii" {
					val = parseNumber(resp[start:end], metric.ParserRule) * metric.Ratio
				} else {
					asciiVal = parseString(resp[start:end])
				}

				constLabels, variableLabels, labelValues := splitLabels(metric.Labels)

				opValue := lookupOptionRangeValue(metric.OptionRanges, int(val))
				labelValues = replacePlaceholders(labelValues, int(val), opValue, asciiVal)

				name := prometheus.BuildFQName(namespace, directory.Directory, metric.MetricName)
				desc := prometheus.NewDesc(name, metric.MetricHelp, variableLabels, constLabels)
				ch <- prometheus.MustNewConstMetric(
					desc,
					parseValueType(metric.MetricType),
					val,
					labelValues...,
				)

				// TODO: make this print optional.
				fmt.Println(metric.Title, val, metric.Unit, asciiVal)
			}
		}
	}
	return up
}

// parseNumber parses the given byte slice according to rule into a
// number. It returns 0 for invalid values of rule, valid values are
// uint16, int16, uint32, and int32.
func parseNumber(bs []byte, rule string) float64 {
	switch rule {
	case "uint16":
		return float64(binary.BigEndian.Uint16(bs))
	// Probably overflows? And hence gives correct result?
	case "int16":
		return float64(int16(binary.BigEndian.Uint16(bs)))
	case "uint32":
		return float64(binary.BigEndian.Uint32(bs))
	// Probably overflows? And hence gives correct result?
	case "int32":
		return float64(int32(binary.BigEndian.Uint32(bs)))
	default:
		fmt.Println(rule, "parsing rule is not supported")
		return 0
	}
}

// parseString parses given byte slice into a string.
func parseString(bs []byte) string {
	return string(bs)
}

// parseValueType returns Prometheus ValueType corresponding to given
// string. It returns UntypedValue for other values of s than "gauge"
// and "counter".
func parseValueType(s string) prometheus.ValueType {
	switch s {
	case "gauge":
		return prometheus.GaugeValue
	case "counter":
		return prometheus.CounterValue
	default:
		return prometheus.UntypedValue
	}
}

// splitLabels takes the labels map and returns constLabels,
// variableLabels, labelValues suitable for using with NewDesc,
// NewConstMetric etc.
func splitLabels(labels map[string]string) (constLabels map[string]string, variableLabels []string, labelValues []string) {
	constLabels = make(map[string]string)
	for k, v := range labels {
		if strings.HasPrefix(v, "<") && strings.HasSuffix(v, ">") {
			variableLabels = append(variableLabels, k)
			labelValues = append(labelValues, v)
			continue
		}
		constLabels[k] = v
	}
	return constLabels, variableLabels, labelValues
}

// lookupOptionRangeValue returns the value of first optionRange whose
// key matches given number. It returns "unknown" if the key is not
// found.
func lookupOptionRangeValue(ors []config.OptionRange, n int) string {
	for _, or := range ors {
		if or.Key == n {
			return or.Value
		}
	}
	return "unknown"
}

// replacePlaceholders substitutes placeholders from the string slice
// with given arguments. Placeholders are surrounded by angular
// brackets. Supported placeholders are <option_value>, <option_key>,
// and <ascii_value>. It returns the modified string slice.
func replacePlaceholders(ss []string, optionKey int, optionVal string, asciiVal string) []string {
	var result []string
	for _, s := range ss {
		if s == "<option_value>" {
			result = append(result, optionVal)
			continue
		}
		if s == "<option_key>" {
			result = append(result, fmt.Sprint(optionKey))
			continue
		}
		if s == "<ascii_value>" {
			result = append(result, asciiVal)
			continue
		}
		if strings.HasPrefix(s, "<") && strings.HasSuffix(s, ">") {
			// TODO: log warning
			fmt.Println(s, "placeholder not implemented")
		}
		result = append(result, s)
	}
	return result
}
