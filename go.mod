module gitlab.com/bhavin192/lsx-exporter

go 1.16

require (
	github.com/prometheus/client_golang v1.11.0
	github.com/sigurn/crc16 v0.0.0-20160107003519-da416fad5162
	github.com/sigurn/utils v0.0.0-20190728110027-e1fefb11a144 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
