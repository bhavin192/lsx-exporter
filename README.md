# LSx Exporter

Prometheus exporter for solar inverter metrics exposed by the logging
sticks like LSW-3, LSE-3, LSG-3 etc.

Based on the project https://github.com/MichaluxPL/Sofar_LSW3 Has only been tested with logging stick having serial number 17xxxxxxxx.

The project status is experimental, the configuration format, metric
names can change heavily.

## Build and Run

Clone the repository and build the exporter:

```sh
go build .
```

Modify config.yaml to have adderss of the inverter logging stick, and
run the exporter.

```sh
./lsx-exporter
```
