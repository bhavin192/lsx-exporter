package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config is the exporter configuration.
type Config struct {
	// Inverter specifies the configuration of inverter and
	// logging stick to scrape.
	Inverter InverterConfig `yaml:"inverter"`

	// RegisterGroups specify register groups to fetch from the
	// inverter.
	RegisterGroups []RegisterGroup `yaml:"register_groups"`
}

// InverterConfig is configuration of the inverter and the logging
// stick connected to it.
type InverterConfig struct {
	// Address specifies the IP address and port of the logging
	// stick.
	//
	// Address must be of the form "ip:port" as accepted by
	// net.Dial address argument.
	//
	// For example, "192.168.1.42:8899".
	Address string `yaml:"address"`

	// SerialNumber specifies the serial number of the logging
	// stick connected to the inverter.
	SerialNumber uint32 `yaml:"serial"`

	// Modbus is the Modbus (slave) address set in the
	// inverter. It is used to identify an inverter when multiple
	// inverters are daisy chained together.
	//
	// When unset, the value defaults to "1".
	Modbus uint8 `yaml:"modbus,omitempty"`
}

// RegisterGroup is a group of all the registers from multiple
// directories.
//
// All the registers from StartAddr to EndAddr are fetched using the
// FnCode. The metric values from these registers are interpreted
// according to the directory configuration of the specified
// Directories.
type RegisterGroup struct {
	// Name specifies the name of the group.
	Name string `yaml:"name"`

	// StartAddr specifies the address of the first register from
	// the group.
	StartAddr uint16 `yaml:"start_address"`

	// EndAddr specifies the address of the last register from the
	// group.
	EndAddr uint16 `yaml:"end_address"`

	// FnCode specifies the Modbus function code to use for
	// fetching the set of registers from StartAddr to EndAddr.
	FnCode uint8 `yaml:"function_code"`

	// Directories specify the directory names from the directory
	// configuration which are part of this group.
	//
	// All the register addresses from the specified directories
	// should be within StartAddr and EndAddr.
	Directories []string `yaml:"directories"`
}

// LoadConfigFile parses the given YAML file into a config and adds
// default values
func LoadConfigFile(filename string) (Config, error) {
	var cfg Config
	fb, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, fmt.Errorf("failed to read config file: %w", err)
	}
	err = yaml.Unmarshal(fb, &cfg)
	if err != nil {
		return cfg, fmt.Errorf("failed to parse config: %w", err)
	}
	// TODO: move this to defaulting method
	if cfg.Inverter.Modbus == 0 {
		cfg.Inverter.Modbus = 0x01
	}

	return cfg, nil
}

// ValidateConfig performs validation checks on given config.
func ValidateConfig(cfg Config) error {
	if cfg.Inverter.Address == "" {
		return fmt.Errorf("Inverter.Address: Must not be empty")
	}
	if cfg.Inverter.SerialNumber == 0 {
		return fmt.Errorf("Inverter.SerialNumber: Must not be empty")
	}
	if len(cfg.RegisterGroups) == 0 {
		return fmt.Errorf("Registers: Must not be empty")
	}
	for rgi, regGroup := range cfg.RegisterGroups {
		if regGroup.EndAddr <= regGroup.StartAddr {
			return fmt.Errorf("Registers[%d].EndAddr: Invalid value: \"%#x\" <= \"%#x\" (StartAddr): "+
				"EndAddr must be greater than StartAddr",
				rgi, regGroup.EndAddr, regGroup.StartAddr)
		}
		if len(regGroup.Directories) == 0 {
			return fmt.Errorf("Registers[%d].Directories: Must not be empty", rgi)
		}
	}
	return nil
}
