package config

import (
	_ "embed"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/bhavin192/lsx-exporter/util"
)

// Directory is a subsystem of metrics supported by an inverter.
//
// Various metric values are stored in the inverter registers. All the
// register locations from smallest to largest location of metrics
// within a Directory must be accessible.
//
// For example, if a Directory has smallest register 0x0001 and the
// largest register 0x000F, then all the register locations from
// 0x0001 to 0x000F of the inverter must be accessible.
type Directory struct {
	// Directory specifies the name of the subsystem.
	Directory string `json:"directory"`

	// Items specify a list of metrics which are part of this
	// directory.
	Items []Item `json:"items"`
}

// Item is a metric.
type Item struct {
	// Title specifies human readable name or short description of
	// the metric.
	Title string `json:"titleEN"`

	// Registers specify inverter registers where the metric value
	// is present.
	//
	// Registers must be a non empty sequential list of
	// numbers. Each register string must start with 0x followed
	// by a 4 digit hex location i.e. a number from 0x0000-0xFFFF.
	//
	// The valid size of this list depends on the ParserRule:
	//   size  ParserRule
	//   1     unit16, int16
	//   2     uint32, int32
	//   1+    ascii
	//
	// Valid example value in case of int32 ParserRule: ["0x0009",
	// "0x000A"].
	Registers []string `json:"registers"`

	// ParserRule specifies how to interpret and parse the value
	// from the Registers.
	//
	// Valid ParserRules are: "uint16", "int16", "unit32", "int32"
	// and "ascii".
	ParserRule string `json:"parserRule"`

	// OptionRanges specify list of mappings from metric value to
	// a string.
	OptionRanges []OptionRange `json:"optionRanges,omitempty"`

	// Ratio specifies a number to multiply the metric value with.
	Ratio float64 `json:"ratio"`

	// Unit specifies a human readable unit of the metric.
	Unit string `json:"unit"`

	// MetricType specifies the Prometheus metric type.
	//
	// Valid values are: "counter" and "gauge".
	MetricType string `json:"metricType"`

	// MetricName specifies the Prometheus metric name.
	MetricName string `json:"metricName"`

	// MetricHelp specifies the Prometheus metric help string.
	MetricHelp string `json:"metricHelp"`

	// Labels specify Prometheus labels, which are mappings of
	// name to value.
	//
	// The label values support following placeholders:
	//   <option_value>  Value from the optionRange.
	//   <option_key>    Key from the optionRange.
	//   <ascii_value>   The parsed string value from the Registers.
	Labels map[string]string `json:"labels"`
}

// OptionRange is a key value pair.
type OptionRange struct {
	// Key specifies a numeric key.
	Key int `json:"key"`

	// Value specifies the meaning of Key. These are messages,
	// errors, states, in the string form.
	Value string `json:"valueEN"`
}

//go:embed directories.json
var defaultDirs []byte

// GetDefaultDirectories parses the embedded directories.json into a
// directory slice.
func GetDefaultDirectories() ([]Directory, error) {
	return LoadDirectories(defaultDirs)
}

// LoadDirectoriesFile parses given filename into a directory slice.
func LoadDirectoriesFile(filename string) ([]Directory, error) {
	fb, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("failed to read directories file: %w", err)
	}
	return LoadDirectories(fb)
}

// LoadDirectories parses byte slice b into a directory slice.
func LoadDirectories(b []byte) ([]Directory, error) {
	var directories []Directory
	err := json.Unmarshal(b, &directories)
	if err != nil {
		err = fmt.Errorf("failed to parse directory configuration: %w", err)
	}
	return directories, err
}

// ValidateDirectories performs validation checks on given directory
// slice.
func ValidateDirectories(dirs []Directory) error {
	validParserRules := []string{"uint16", "int16", "uint32", "int32", "ascii"}
	validRegistersLens := map[string]int{
		"uint16": 1,
		"int16":  1,
		"uint32": 2,
		"int32":  2,
	}
	for di, dir := range dirs {
		if dir.Directory == "" {
			return fmt.Errorf("[%d].Directory: Must not be empty", di)
		}
		for ii, item := range dir.Items {
			if item.MetricName == "" {
				return fmt.Errorf("%s.Items[%d].MetricName: Must not be empty",
					dir.Directory, ii)
			}

			loc := fmt.Sprintf("%s.%s", dir.Directory, item.MetricName)
			if !util.StringInSlice(item.ParserRule, validParserRules) {
				return fmt.Errorf("%s.ParserRule: Invalid value: %q: must be one of the %q",
					loc, item.ParserRule, validParserRules)
			}
			if len(item.Registers) == 0 {
				return fmt.Errorf("%s.Registers: Must not be empty", loc)
			}
			if item.ParserRule != "ascii" &&
				len(item.Registers) != validRegistersLens[item.ParserRule] {
				return fmt.Errorf("%s.Registers: Invalid length %d: must be %d registers for %q ParserRule",
					loc, len(item.Registers),
					validRegistersLens[item.ParserRule], item.ParserRule)
			}
			// TODO: check if the numbers are actually in
			// sequence?
			for ri, r := range item.Registers {
				if err := validateRegister(r); err != nil {
					return fmt.Errorf("%s.Registers[%d]: %w", loc, ri, err)
				}
			}
		}
	}
	return nil
}

func validateRegister(r string) error {
	if !strings.HasPrefix(r, "0x") {
		return fmt.Errorf("Invalid value: %q: register must start with 0x", r)
	}
	if len(r) != 6 {
		return fmt.Errorf("Invalid value: %q: register must be a 4 digit hex, got %d",
			r, len(r)-2)
	}
	if _, err := hex.DecodeString(r[2:]); err != nil {
		return fmt.Errorf("Invalid value %q: register must be a valid hex number: %w",
			r, err)
	}
	return nil
}
