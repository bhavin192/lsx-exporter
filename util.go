package main

func getNumberHiLo(n uint16) []byte {
	hi := n >> 8 & 0xFF
	lo := n & 0xFF
	return []byte{byte(hi), byte(lo)}
}

func reverseByteSlice(s []byte) []byte {
	var r []byte
	for i := len(s) - 1; i >= 0; i-- {
		r = append(r, s[i])
	}
	return r
}

// checkSum8Modulo256 calculates the CheckSum8 Modulo 256 of the slice
// bs. Modulo 256 is basically keeping least significant 255 bits from
// the number. https://stackoverflow.com/a/24415114
func checkSum8Modulo256(bs []byte) byte {
	var sum byte
	for _, n := range bs {
		// To avoid overflow, though overflow is fine in this
		// use case.
		sum += n & 255
	}
	return sum & 255
}
