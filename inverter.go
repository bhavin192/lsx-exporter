package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"net"

	"github.com/sigurn/crc16"
)

// inverter holds the details about an inverter or the connected
// logging stick. Provides functions to communicate with the inverter.
type inverter struct {
	// Address is the IP address of the inverter along with the
	// port as accepted by the net.Dial address argument.
	Address string
	// SN is the serial number of the logging stick in the reverse
	// order.
	SN []byte
	// modbusAddr is the modbus (slave) adderss set in the
	// inverter. Used to identify an inverter when multiple
	// inverters are daisy chained together.
	modbusAddr uint8
}

func (i *inverter) GetRegisters(fnCode uint8, regStartAddr, regEndAddr uint16) ([]byte, error) {
	// TODO: remove this.
	fmt.Printf("fn: %x, start: 0x%x, end: 0x%x\n", fnCode, regStartAddr, regEndAddr)
	// TODO: buildFrame function?

	var frame []byte
	// TODO: all of these DecodeString can be changed with
	// something like []byte{0xA5, 0x17} and so on.
	startMarker, _ := hex.DecodeString("A5")
	frame = append(frame, startMarker...)
	dataLengthLoHi, _ := hex.DecodeString("1700")
	frame = append(frame, dataLengthLoHi...)
	controlCodeLoHi, _ := hex.DecodeString("1045")
	frame = append(frame, controlCodeLoHi...)
	serialNumber, _ := hex.DecodeString("0000")
	frame = append(frame, serialNumber...)
	frame = append(frame, i.SN...)
	dataField, _ := hex.DecodeString("020000000000000000000000000000")
	frame = append(frame, dataField...)

	var modbusBusinessField []byte
	modbusAddrAndFunctionCode := []byte{i.modbusAddr, fnCode}
	modbusBusinessField = append(modbusBusinessField, modbusAddrAndFunctionCode...)
	modbusBusinessField = append(modbusBusinessField, getNumberHiLo(regStartAddr)...)
	// modbus number of registers
	modbusBusinessField = append(modbusBusinessField, getNumberHiLo(regEndAddr-regStartAddr+1)...)
	fmt.Println("modbusBusinessField: ")
	fmt.Println(hex.Dump(modbusBusinessField))
	frame = append(frame, modbusBusinessField...)
	modbusCRC := crc16.Checksum(modbusBusinessField, crc16.MakeTable(crc16.CRC16_MODBUS))
	frame = append(frame, reverseByteSlice(getNumberHiLo(modbusCRC))...)
	frame = append(frame, checkSum8Modulo256(frame[1:]))
	endMarker, _ := hex.DecodeString("15")
	frame = append(frame, endMarker...)

	fmt.Println("Formed frame: ")
	fmt.Println(hex.Dump(frame))

	// TODO: also check if the response is an error one or not.
	return i.Request(frame)

	// TODO: remove this later
	mockResp := []byte{165, 99, 0, 16, 21, 0, 4, 172, 186, 54, 105, 2, 1, 159, 112, 0, 0, 225, 1, 0, 0, 0, 0, 0, 0, 1, 3, 80, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 123, 0, 122, 0, 10, 0, 0, 0, 32, 0, 0, 0, 24, 0, 0, 19, 130, 9, 170, 0, 96, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 78, 0, 0, 1, 23, 6, 75, 2, 118, 0, 48, 0, 58, 15, 60, 10, 123, 0, 10, 0, 60, 0, 0, 0, 1, 0, 0, 2, 120, 46, 224, 46, 224, 0, 22, 248, 118, 143, 21}
	fmt.Println("sending mock response!")
	return mockResp, nil
}

func (i *inverter) Request(frame []byte) ([]byte, error) {
	resp := make([]byte, 1024)

	// TODO: improve these error messages
	conn, err := net.Dial("tcp", i.Address)
	if err != nil {
		return resp, fmt.Errorf("error dialing: %w", err)
	}
	defer conn.Close()

	_, err = conn.Write(frame)
	if err != nil {
		return resp, fmt.Errorf("error writing: %w", err)
	}

	if _, err := conn.Read(resp); err != nil {
		return resp, fmt.Errorf("error reading: %w", err)
	}

	// TODO: remove this
	fmt.Println("Reply:")
	fmt.Println(hex.Dump(resp))
	// fmt.Printf("%v\n", resp)
	return resp, nil
}

// NewInverter creates new inverter object with address, serial number
// sn and modbusAddr. The address needs to be of same format as of
// net.Dial address argument.
func NewInverter(addresss string, sn uint32, modbusAddr uint8) inverter {
	// TODO: add comment about this length
	snb := make([]byte, 4)
	// LittleEndian has LSB first i.e. the reverse order we need.
	binary.LittleEndian.PutUint32(snb, sn)
	return inverter{Address: addresss, SN: snb, modbusAddr: modbusAddr}
}
