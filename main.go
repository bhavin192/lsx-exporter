// LSx Exporter
// Copyright (C) 2022  Bhavin Gandhi
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/bhavin192/lsx-exporter/config"
)

func main() {
	var (
		listenAddress = flag.String("web.listen-address", ":9957",
			"Address on which to expose metrics and web interface.")
		metricsPath = flag.String("web.telemetry-path", "/metrics",
			"Path under which to expose metrics.")
		// TODO: implement this
		// disableExporterMetrics = flag.Bool("web.disable-exporter-metrics", false,
		// 	"Exclude metrics about the exporter itself (promhttp_*, process_*, go_*).")
		configFile = flag.String("config.file", "config.yaml",
			"Exporter configuration file path.")
		directoryConfigFile = flag.String("config.directory-file", "",
			"JSON formatted directories file path.")
		validate = flag.Bool("validate", false,
			"Validate the configuration files and exit.")
		// --version
		// --help

	)
	flag.Parse()

	cfg, err := config.LoadConfigFile(*configFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// fmt.Printf("%+v\n", cfg)
	if err := config.ValidateConfig(cfg); err != nil {
		fmt.Printf("The configuration %q is invalid:\n", *configFile)
		fmt.Println(err)
		os.Exit(1)
	}

	inv := NewInverter(cfg.Inverter.Address, cfg.Inverter.SerialNumber, cfg.Inverter.Modbus)

	var dirs []config.Directory
	if *directoryConfigFile != "" {
		dirs, err = config.LoadDirectoriesFile(*directoryConfigFile)
	} else {
		dirs, err = config.GetDefaultDirectories()
	}
	if err != nil {
		// TODO: do proper logging here.
		fmt.Println(err)
		os.Exit(2)
	}
	if err := config.ValidateDirectories(dirs); err != nil {
		fmt.Println("The directory configuration is invalid:")
		fmt.Println(err)
		os.Exit(1)
	}

	if *validate {
		fmt.Println("Configuration files are valid.")
		os.Exit(0)
	}

	ic := inverterCollector{
		Inverter:       inv,
		Directories:    dirs,
		RegisterGroups: cfg.RegisterGroups,
	}

	reg := prometheus.NewPedanticRegistry()
	// TODO: add process and Go collectors
	reg.MustRegister(&ic)

	http.Handle(*metricsPath, promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
				<head><title>LSx Exporter</title></head>
				<body>
				<h1>LSx Exporter</h1>
				<p><a href="` + *metricsPath + `">Metrics</a></p>
				<body>
				</html>`))
	})
	// TODO: handle any errors to listen.
	http.ListenAndServe(*listenAddress, nil)

	// TODO: once to stdout (process-exporter).

	// TODO: Use log package and have logging.

	// TODO: How to handle the hardware info related metrics.

	// TODO: have scrape_duration_seconds metric? Is it total, or
	// just keeps on changing? Look for some
	// examples. https://prometheus.io/docs/instrumenting/writing_exporters/#metrics-about-the-scrape-itself
}
